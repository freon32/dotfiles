;=====================================================
;	freon's polybar
;	adapted from default + antium 
;	antium credit: github.com/vivacaligula
;=====================================================

;=====================================================
;			COLORS
;=====================================================
[colors]
xcolor0 = ${xrdb:color0}
xcolor1 = ${xrdb:color1}
xcolor2 = ${xrdb:color2}
xcolor3 = ${xrdb:color3}
xcolor4 = ${xrdb:color4}
xcolor5 = ${xrdb:color5}
xcolor6 = ${xrdb:color6}
xcolor7 = ${xrdb:color7}
xcolor8 = ${xrdb:color8}
xcolor9 = ${xrdb:color9}
xcolor10 = ${xrdb:color10}
xcolor11 = ${xrdb:color11}
xcolor12 = ${xrdb:color12}
xcolor13 = ${xrdb:color13}
xcolor14 = ${xrdb:color14}
xcolor15 = ${xrdb:color15}

primary = ${colors.xcolor1} 
; #ffb52a
secondary = ${colors.xcolor8} 
; #e60053
alert = ${colors.xcolor11} 
; #bd2c40

background = ${colors.xcolor0}
background-alt = ${colors.xcolor8}
; 444
foreground = ${colors.xcolor7}
foreground-alt = ${colors.xcolor8}
; 555

;=====================================================
;       		GLOBAL
;=====================================================
[bar/example]
;monitor = ${env:MONITOR:LVDS-1}
width = 100%
height = 24
offset-x = 0%
offset-y = 0%
radius = 0
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 2

module-margin-left = 1
module-margin-right = 1

;=====================================================
;			FONTS
;=====================================================
font-0 = "xos4 Terminus:pixelsize=10;1"
font-1 = "unifont:fontformat=truetype:size=8:antialias=false;0"
font-2 = "Wuncon Siji:pixelsize=10;1"

;=====================================================
;		   MODULES + TRAY
;=====================================================
modules-left = i3
modules-center = spotify
modules-right = volume custombacklight cpu memory temperature wlan eth battery date

tray-position = right
tray-padding = 2
;tray-transparent = true
;tray-background = #0063ff

;=====================================================
;		    WM SPECIFICS
;=====================================================
;wm-restack = bspwm
wm-restack = i3

;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

;=====================================================
;		       XWINDOW
;=====================================================
[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

;=====================================================
;	X K E Y B O A R D
;=====================================================
[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.secondary}

label-layout = %layout%
label-layout-underline = ${colors.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

;=====================================================
;	F I L E S Y S T E M
;=====================================================
[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted = %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

;=====================================================
;	B S P W M
;=====================================================
[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.xcolor2}
label-focused-underline= ${colors.xcolor3}
label-focused-padding = 1

label-occupied = %index%
label-occupied-padding = 1

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 1

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 1

;=====================================================
;	I 3 W M
;=====================================================
[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 1
label-mode-foreground = ${colors.primary}
label-mode-background = ${colors.secondary}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-underline = ${module/bspwm.label-focused-underline}
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

;=====================================================
;	M P D
;=====================================================
[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true

;=====================================================
;	X B A C K L I G H T (does not work)
;=====================================================
[module/xbacklight]
type = internal/xbacklight
format = <label> <bar>
label = BL

bar-width = 10
bar-indicator = |
bar-indicator-foreground = #ff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}

;=====================================================
;	B A C K L I G H T A C P I (also does not work)
;=====================================================
[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = acpi_video0

;=====================================================
;	C P U
;=====================================================
[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.xcolor1}
label = %percentage:2%%

;=====================================================
;	M E M O R Y
;=====================================================
[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-underline = ${colors.xcolor2}
label = %percentage_used%%

;=====================================================
;	W L A N
;=====================================================
[module/wlan]
type = internal/network
interface = wlp3s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = ${colors.xcolor4}
label-connected = %essid%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

ramp-signal-0 = 
ramp-signal-1 = 
ramp-signal-2 = 
ramp-signal-3 = 
ramp-signal-4 = 
ramp-signal-foreground = ${colors.foreground-alt}

;=====================================================
;	E T H E R N E T
;=====================================================
[module/eth]
type = internal/network
interface = enp0s25
interval = 3.0

format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = %local_ip%

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

;=====================================================
;	D A T E / T I M E
;=====================================================
[module/date]
type = internal/date
interval = 5


date = "%Y/%m/%d "
time = " %I:%M:%S %p"
;time-alt = " %z %Z. "

;format-prefix = 
;format-prefix-foreground = ${colors.foreground-alt}
;format-underline = #0a6cf5

label = %time%

;=====================================================
;	V O L U M E
;=====================================================
[module/volume]
type = internal/volume

format-volume = <label-volume>
label-volume =  %percentage%%
label-volume-foreground = ${root.foreground}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground-alt}
label-muted = %percentage%%

bar-volume-width = 10
bar-volume-foreground-0 = #55aa55
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #55aa55
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

;=====================================================
;	B A T T E R Y
;=====================================================
[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98

format-charging = <animation-charging> <label-charging>
format-charging-underline = ${colors.xcolor5}

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = ${self.format-charging-underline}

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750

;=====================================================
;	T E M P E R A T U R E
;=====================================================
[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-underline = ${colors.xcolor3}
format-warn = <ramp> <label-warn>
format-warn-underline = ${self.format-underline}

label = %temperature%
label-warn = %temperature%
label-warn-foreground = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground-alt}

;=====================================================
;	P O W E R M E N U
;=====================================================
[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.secondary}
label-close =  cancel
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = reboot

menu-2-0 = power off
menu-2-0-exec = shutdown -P now
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

;=====================================================
; CUSTOM BACKLIGHT (bc xbacklight won't work w/ modesetting)
;=====================================================
[module/custombacklight]
type = custom/script
exec = ~/.config/polybar/custombacklight.sh | sed '/\./ s/\.\{0,1\}0\{1,\}$//'
format = <label>
format-prefix = " "
label =  %output%%
interval = 1

;=================
;   spotify
;=================
[module/spotify]
type = custom/script
exec = ~/.config/polybar/spotify.sh %artist% - %title%
tail = true
interval = 2

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
