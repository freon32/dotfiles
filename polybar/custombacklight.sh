#!/bin/bash
bright=$( cat /sys/class/backlight/acpi_video0/brightness )
bc -l <<- EOF
	scale = 2
	x = $bright	
	ans = (x / 15) * 100
	print ans, "\n"
EOF
