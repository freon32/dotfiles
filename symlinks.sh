#!/bin/bash

ln -s ~/dotfiles/vim/vimrc ~/.vim/vimrc
ln -s ~/dotfiles/i3/config ~/.config/i3/config
ln -s ~/dotfiles/polybar/config ~/.config/polybar/config
ln -s ~/dotfiles/Xresources ~/.Xresources
ln -s ~/dotfiles/xinitrc ~/.xinitrc
ln -s ~/dotfiles/compton.conf ~/.config/compton.conf
